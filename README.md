# README #

The app is created to control ten aircraft using a server phone with each aircraft connected to a client phone. The app has two mode: server and client. After click on the open button in the main window. The user can choose a server or client mode. 

Server: 
To use the server, first press "START SERVER" button. When the "start?" text below the button turn into "Server len: 15", the server has started waiting for clients to be connected. 

Client (Another phone): 
Before choosing the server mode, make sure that the mobile phone is connected to the remote controller of the aircraft and the mode is shown in the product Information(Phantom 4 etc) on the main window. 
Then, choose client mode from the choiceView. 
Then, enter the server IP address shown on the server side. 
Then, click connect to Server. 

Now, the server and client are connected. 

Server: 
Now press "TAKE OFF ALL" button to take off all client aircrafts. 

Client:
The serverReceptor textview will show take off. 

Server:
Now press "ENABLE ALL" to enable virtual stick mode. Also you can adjust the three seek bar at the bottom (Velocity, Upper Height, Lower Height) to set the desired values for the aircraft. Now you can press the buttons from 1 to 10 to control the aircraft to go up and down. (Press down to go up, and release to go down.)

-----------------------------------------------------------------------------------------------------------------------------------------------

# CODE BREAKDOWN #
All the views are inside com.dji.sdk.sample. The app utilises 5 java classes inside the flightcontroller. 

ChoiceView: view for choosing client or server mode. 
ServerView: view for server. 
VirtualStickView (client): view for client. 
Command: class used to transfer data between server and client. (the class will be turned into a JSON string using GSON)
MyServer: class used by ServerView. Inside there is a private class clientSocket used to associate a list of clients to the server. 

Also the main window view is inside common/MainContent. All layout can be found in res. 

-----------------------------------------------------------------------------------------------------------------------------------------------
 # POSSIBLE IMPROVEMENTS #

1. The app used JSON to encode and decode data between server and client. The data the input stream reads is arbitrary. Therefore, we could encode the JSON string for the command with header and length: HEADER + LENGTH + JSON STRING in bytes. 
2. We could also add indicators that the client has been connected to the server on the server view side.