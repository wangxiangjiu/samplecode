package com.dji.sdk.sample.flightcontroller;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangxiangjiu on 6/20/16.
 */
public class MyServer {

    Thread mThread;
    ServerSocket serverSocket;
    Socket connectedSocket;
    ObjectInputStream in;
    ObjectOutputStream out;

    public static int len;
    /* Port Number. */
    private int portNumber = 2001;


    /* List of clientSockets. */
    List<ClientSocket> clientSockets = new ArrayList<>();

    public MyServer() {
            len = 15;
    }

    public void destroy() {

    }

    public int getPortNumber() {
        return portNumber;
    }




    /**
     * Method that send Message MSG to All Clients.
     * @param msg
     */
    public void sendMsgToAll(String msg)  {
        try {
            for (ClientSocket clientSocket : clientSockets) {
                clientSocket.sendMsg(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that sends Message to specific client.
     * @param clientSocket
     * @param msg
     */
    public void sendMsgToOne(ClientSocket clientSocket, String msg) {
        clientSocket.sendMsg(msg);
    }

    /**
     * Method triggered when pressing the button.
     */
    public void startListening() {

        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(portNumber);
                    while (true) {
                        connectedSocket = serverSocket.accept();
                        ClientSocket clientSocket = new ClientSocket(connectedSocket);
                        clientSocket.start();
                        clientSockets.add(clientSocket);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mThread.start();
    }

    /**
     * ClientSocket is the socket that communicates with the Client.
     */
    public class ClientSocket extends Thread {


        private boolean isVelocity;
        private boolean isUpperHeight;
        private boolean isLowerHeight;

        private Socket socket;
        private InputStream in;
        private OutputStream out;
        protected String messageFromClient;

        public ClientSocket(Socket socket) {
            this.socket = socket;
            try {
                in = this.socket.getInputStream();
                out = this.socket.getOutputStream();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    byte[] buffer = new byte[1024 * 8];
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void sendMsg(String str) {
            try {
                out.write(str.getBytes());
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
