package com.dji.sdk.sample.flightcontroller;

import android.app.Service;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dji.sdk.sample.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Timer;

import dji.thirdparty.gson.Gson;

/**
 * Created by wangxiangjiu on 6/23/16.
 */
public class ServerView extends RelativeLayout implements View.OnClickListener {

    //Gson created. Will be used later.
    public static Gson gson = new Gson();

    private Command cmd0, cmd1, cmd2, cmd3, cmd4, cmd5, cmd6, cmd7;

    private static SeekBar velocityBar, upperHeightBar, lowerHeightBar;
    private static TextView velocity_view, upperHeight_view, lowerHeight_view;

    private float velocityFactor;

    private Button btnStartServer, take_off_all, enable_all, disable_all;
    private Button c1, c2, c3, c4, c5, c6, c7, c8, c9, c10;

    private MyServer server_boss;
    private TextView serverMsg;
    private int seekBarUpperHeight, seekBarLowerHeight;

    private TextView infoip;

    private Timer mSendVirtualStickDataTimer;

    private boolean mOnlyForVerticalFlag;

    private int portNumber = 2001;

    private String jsonTakeOff, jsonEnableVirtualStick, jsonDisableVirtualStick, jsonTouchDown, jsonTouchUp, jsonVelocityFactor, jsonUpperHeight, jsonLowerHeight;
    private String EncodedJsonTakeoff;

    /**
     * The constructor for ServerView takes in context and attrs
     *
     * @param context
     * @param attrs
     */
    public ServerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context, attrs);
    }

    private void initUI(Context context, AttributeSet attrs) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);

        View content = layoutInflater.inflate(R.layout.server, null, false);
        addView(content, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        server_boss = new MyServer();
        btnStartServer = (Button) findViewById(R.id.start_server);
        take_off_all = (Button) findViewById(R.id.take_off_all);
        enable_all = (Button) findViewById(R.id.enable_all);
        disable_all = (Button) findViewById(R.id.disable_all);

        c1 = (Button) findViewById(R.id.bc1);
        c2 = (Button) findViewById(R.id.bc2);
        c3 = (Button) findViewById(R.id.bc3);
        c4 = (Button) findViewById(R.id.bc4);
        c5 = (Button) findViewById(R.id.bc5);
        c6 = (Button) findViewById(R.id.bc6);
        c7 = (Button) findViewById(R.id.bc7);
        c8 = (Button) findViewById(R.id.bc8);
        c9 = (Button) findViewById(R.id.bc9);
        c10 = (Button) findViewById(R.id.bc10);

        serverMsg = (TextView) findViewById(R.id.serverMsg);

        infoip = (TextView) findViewById(R.id.infoip);




        infoip.setText(getIpAddress());

        //Take Off
        cmd3 = new Command();
        cmd3.cmdId = 3;
        jsonTakeOff = gson.toJson(cmd3);
        jsonEncode(jsonTakeOff.getBytes());

        //Enable Virtual Stick
        cmd4 = new Command();
        cmd4.cmdId = 4;
        jsonEnableVirtualStick = gson.toJson(cmd4);

        //Disable Virtual Stick
        cmd5= new Command();
        cmd5.cmdId = 5;
        jsonDisableVirtualStick = gson.toJson(cmd5);

        //Touch Down
        cmd0= new Command();
        cmd0.cmdId = 0;
        jsonTouchDown = gson.toJson(cmd0);

        //Touch Up
        cmd1 = new Command();
        cmd1.cmdId = 1;
        jsonTouchUp = gson.toJson(cmd1);

        //Velocity Factor
        cmd2 = new Command();
        cmd2.cmdId = 2;

        //Upper Height
        cmd6 = new Command();
        cmd6.cmdId = 6;
       // jsonUpperHeight = gson.toJson(cmd6);

        //Lower Height
        cmd7 = new Command();
        cmd7.cmdId = 7;
       // jsonLowerHeight = gson.toJson(cmd7);

        setVelocityBar();
        setLowerHeightBar();
        setUpperHeightBar();


        c1.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(0), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(0), jsonTouchUp);
                }
                return false;
            }
        });

        c2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(1), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(1), jsonTouchUp);
                }
                return false;
            }
        });

        c3.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(2), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(2), jsonTouchUp);
                }
                return false;
            }
        });

        c4.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(3), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(3), jsonTouchUp);
                }
                return false;
            }
        });

        c5.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(4), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(4), jsonTouchUp);
                }
                return false;
            }
        });

        c6.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(5), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(5), jsonTouchUp);
                }
                return false;
            }
        });

        c7.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(6), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(6), jsonTouchUp);
                }
                return false;
            }
        });

        c8.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(7), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(7), jsonTouchUp);
                }
                return false;
            }
        });

        c9.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(8), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(8), jsonTouchUp);
                }
                return false;
            }
        });

        c10.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mSendVirtualStickDataTimer = new Timer();
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(9), jsonTouchDown);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    server_boss.sendMsgToOne(server_boss.clientSockets.get(9), jsonTouchUp);
                }
                return false;
            }
        });

        btnStartServer.setOnClickListener(this);
        take_off_all.setOnClickListener(this);
        enable_all.setOnClickListener(this);
        disable_all.setOnClickListener(this);

    }

    /**
     * Method that set the velocity seekbar in the Serverview.
     */
    public void setVelocityBar() {
        velocityBar = (SeekBar) findViewById(R.id.serverVelocity);
        velocity_view = (TextView) findViewById(R.id.serverVelocityView);
        velocity_view.setText("Velocity: " + velocityBar.getProgress() + " / " + velocityBar.getMax());

        velocityBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress_value;

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {

                            progress_value = progress;
                            velocity_view.setText("Velocity: " + progress + " / " + velocityBar.getMax());
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        velocityFactor = (Float) ((float) progress_value / (float) velocityBar.getMax());
                        cmd2.value = velocityFactor;
                        jsonVelocityFactor = gson.toJson(cmd2);
                        server_boss.sendMsgToAll(jsonVelocityFactor);
                        velocity_view.setText("Velocity: " + progress_value + " / " + velocityBar.getMax());
                    }
                }
        );
    }

    /**
     * Method for setting up the Upper Height Bar.
     */
    public void setUpperHeightBar() {
        upperHeightBar = (SeekBar) findViewById(R.id.ServerUpperHeight);
        upperHeight_view = (TextView) findViewById(R.id.serverUpperHeightView);
        upperHeight_view.setText("Upper Height: " + upperHeightBar.getProgress() + " / " + upperHeightBar.getMax());

        upperHeightBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    //int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            seekBarUpperHeight = progress;
                            upperHeight_view.setText("Upper Height: " + progress + " / " + upperHeightBar.getMax());
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        cmd6.height = seekBarUpperHeight;
                        jsonUpperHeight = gson.toJson(cmd6);
                        server_boss.sendMsgToAll(jsonUpperHeight);
                        upperHeight_view.setText("Upper Height: " + seekBarUpperHeight + " / " + upperHeightBar.getMax());
                    }
                }
        );
    }

    /**
     * Method for setting the Lower Height Bar.
     */
    public void setLowerHeightBar() {
        lowerHeightBar = (SeekBar) findViewById(R.id.ServerLowerHeight);
        lowerHeight_view = (TextView) findViewById(R.id.serverLowerHeightView);
        lowerHeight_view.setText("Lower Height :" + lowerHeightBar.getProgress() + " / " + lowerHeightBar.getMax());

        lowerHeightBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    //int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            seekBarLowerHeight = progress;

                            lowerHeight_view.setText("Lower Height: " + progress + " / " + lowerHeightBar.getMax());
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        cmd7.height = seekBarLowerHeight;
                        jsonLowerHeight = gson.toJson(cmd7);
                        server_boss.sendMsgToAll(jsonLowerHeight);
                        lowerHeight_view.setText("Lower Height: " + seekBarLowerHeight + " / " + lowerHeightBar.getMax());
                    }
                }
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_server:
                //btnStartServer.setBackgroundColor(Color.BLUE);
                server_boss.startListening();
                for(MyServer.ClientSocket i :server_boss.clientSockets) {
                    if (i.messageFromClient.equals("client")) {
                        c1.setBackgroundColor(Color.BLUE);
                        //c1.setText("connected");
                    }
                }
                serverMsg.setText("Server len: " + server_boss.len);
                //serverMsg.setText("Server Started");
                break;
            case R.id.take_off_all:
                //server_boss.sendMsgToAll(new String(jsonEncode(jsonTakeOff.getBytes())));
                server_boss.sendMsgToAll(jsonTakeOff);
                break;
            case R.id.enable_all:
                server_boss.sendMsgToAll(jsonEnableVirtualStick);
                break;
            case R.id.disable_all:
                server_boss.sendMsgToAll(jsonDisableVirtualStick);
            default:
                break;
        }

    }

    /**
     * Method that encode the Json String. FORMAT: HEADER + LENGTH + JSONSTRING (in bytes)
     * @param byteArray
     * @return
     */
    private static byte[] jsonEncode(byte[] byteArray) {
        byte[] res = new byte[byteArray.length + 8];
        byte[] header = new byte[] { 0x34, 0x4A, 0x41, 0x56 };
        byte[] length = intToByteArray(byteArray.length);
        System.arraycopy(header, 0, res, 0, header.length);
        System.arraycopy(length, 0, res, 4, length.length);
        System.arraycopy(byteArray, 0, res, 8, byteArray.length);
        return res;

    }

    /**
     * Method that turns an int into the corresponding byte array.
     * @param a
     * @return
     */
    public static byte[] intToByteArray(int a)
    {
        byte[] ret = new byte[4];
        ret[3] = (byte) (a & 0xFF);
        ret[2] = (byte) ((a >> 8) & 0xFF);
        ret[1] = (byte) ((a >> 16) & 0xFF);
        ret[0] = (byte) ((a >> 24) & 0xFF);
        return ret;
    }


    /**
     * Returns the IP Address of the device.
     *
     * @return
     */
    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                                + inetAddress.getHostAddress() + "\n";
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }
}
