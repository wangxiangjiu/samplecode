package com.dji.sdk.sample.flightcontroller;

import android.app.Service;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dji.sdk.sample.R;
import com.dji.sdk.sample.common.DJISampleApplication;
import com.dji.sdk.sample.common.Utils;
import com.dji.sdk.sample.utils.DJIModuleVerificationUtil;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import dji.sdk.FlightController.DJIFlightControllerDataType;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIError;

/**
 * Class for virtual stick.
 */
public class VirtualStickView extends RelativeLayout implements View.OnClickListener {

    private boolean mYawControlModeFlag = true;
    private boolean mRollPitchControlModeFlag = true;
    private boolean mVerticalControlModeFlag = true;
    private boolean mHorizontalCoordinateFlag = true;
    private boolean mStartSimulatorFlag = false;

    private Button mBtnEnableVirtualStick;
    private Button mBtnDisableVirtualStick;
    private Button mBtnHorizontalCoordinate;
    private Button mBtnSetYawControlMode;
    private Button mBtnSetVerticalControlMode;
    private Button mBtnSetRollPitchControlMode;
    private ToggleButton mBtnSimulator;
    private Button mBtnTakeOff;
    private Button mBtnTestUp;
    private Button mBtnClear;


    private Button btn_connect_to_server;

    private TextView mTextView;
    private TextView mSmallText;
    private TextView clientInfoIp;

    private TextView byteArray;

    TextView textResponse;
    private EditText editTextAddress, editTextPort;
    private Button buttonClear;

    private Timer mSendVirtualStickDataTimer;
    private SendVirtualStickDataTask mSendVirtualStickDataTask;
    private SendVirtualStickDataTaskOnlyForVertical mSendVirtualStickDataTaskOnlyForVertical;

    private float velocityFactor;
    private float seekBarUpperHeight;
    private float seekBarLowerHeight;
    private float mPitch;
    private float mRoll;
    private float mYaw;
    private float mThrottle;
    Handler mhandler = new Handler();

    /**
     * The height that is 3 meters above the safety Upper Height.
     */
    private float ultimateUpperHeight;
    private float safetyUpperHeight;
    /**
     * The height that is 3 meters below the saftey Lower Height.
     */
    private float ultimateLowerHeight;
    private float safetyLowerHeight;


    private float mCurrentAltitude;
    private boolean mOnlyForVerticalFlag = false;
    private boolean mFlyingUp = false;
    private Socket clientSocket;
    private InputStream in = null;
    private OutputStream os = null;
    private TextView serverReceptor;
    protected final byte[] header = new byte[]{0x44, 0x4A, 0x41, 0x56};

    public VirtualStickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context, attrs);
    }

    /**
     * Handler that enables message to be send in the UI thread.
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            messageDisplay((String) msg.obj);
        }
    };

    public void messageDisplay(String servermessage) {
        serverReceptor.setText("" + servermessage);
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (null != mSendVirtualStickDataTimer) {
            mSendVirtualStickDataTaskOnlyForVertical.cancel();
            mSendVirtualStickDataTaskOnlyForVertical = null;
            mSendVirtualStickDataTimer.cancel();
            mSendVirtualStickDataTimer.purge();
            mSendVirtualStickDataTimer = null;
        }
    }

    private void initUI(Context context, AttributeSet attrs) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);

        View content = layoutInflater.inflate(R.layout.view_virtual_stick, null, false);
        addView(content, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        mBtnEnableVirtualStick = (Button) findViewById(R.id.btn_enable_virtual_stick);
        mBtnDisableVirtualStick = (Button) findViewById(R.id.btn_disable_virtual_stick);
        mBtnTakeOff = (Button) findViewById(R.id.btn_take_off);
        btn_connect_to_server = (Button) findViewById(R.id.client_connect_to_server);
        mSmallText = (TextView) findViewById(R.id.textView4);
        clientInfoIp = (TextView) findViewById(R.id.clientIP);
        mBtnTestUp = (Button) findViewById(R.id.btn_test_up);
        mBtnClear = (Button) findViewById(R.id.clear);

        editTextAddress = (EditText) findViewById(R.id.address);

        /* Text reminding the client that it has been connected to the server. */
        serverReceptor = (TextView) findViewById(R.id.client_server_receptor);
        clientInfoIp.setText(getIpAddress());

        editTextAddress = (EditText) findViewById(R.id.address);

        mBtnEnableVirtualStick.setOnClickListener(this);
        mBtnDisableVirtualStick.setOnClickListener(this);

        mBtnTakeOff.setOnClickListener(this);
        btn_connect_to_server.setOnClickListener(this);
        mBtnClear.setOnClickListener(this);


        mBtnTestUp.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mOnlyForVerticalFlag = true;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    touchDown();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    touchUp();
                }

                return false;
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_enable_virtual_stick:
                enableVirtualStick();
                break;
            case R.id.btn_disable_virtual_stick:
                disableVirtualStick();
                break;
            case R.id.btn_take_off:
                takeoff();
                break;
            case R.id.clear:
                break;
            case R.id.client_connect_to_server:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String dstAddress = editTextAddress.getText().toString();
                            clientSocket = new Socket(dstAddress, 2001);
                            in = clientSocket.getInputStream();
                            os = clientSocket.getOutputStream();
                            int len = 0;
                            byte[] copy;
                            String receivedJsonString;
                            Command cmd;
                            try {
                                byte[] buffer = new byte[1024 * 8];
                                while (true) {
                                    len = in.read(buffer);
                                    while (len != -1) {
                                        copy = new byte[len];
                                        System.arraycopy(buffer, 0, copy, 0, len);
                                        receivedJsonString = new String(copy);
                                        Message serverReceptor = Message.obtain();
                                        cmd = ServerView.gson.fromJson(receivedJsonString, Command.class);
                                        switch (cmd.cmdId) {
                                            case 3:
                                                takeoff();
                                                serverReceptor.obj = "Take Off";
                                                break;
                                            case 4:
                                                enableVirtualStick();
                                                serverReceptor.obj = "Enable Virtual Stick";
                                                break;
                                            case 5:
                                                disableVirtualStick();
                                                serverReceptor.obj = "Disable Virtual Stick";
                                                break;
                                            case 0:
                                                touchDown();
                                                serverReceptor.obj = "Touch Down";
                                                break;
                                            case 1:
                                                touchUp();
                                                serverReceptor.obj = "Touch Up";
                                                break;
                                            case 2:
                                                velocityFactor = cmd.value;
                                                serverReceptor.obj = "trying to get velocityFactor" + velocityFactor;
                                                break;
                                            case 6:
                                                seekBarUpperHeight = cmd.height;
                                                serverReceptor.obj = "trying to get Upper Height" + seekBarUpperHeight;
                                                break;
                                            case 7:
                                                seekBarLowerHeight = cmd.height;
                                                serverReceptor.obj = "trying to get Lower Height" + seekBarLowerHeight;
                                                break;
                                            default:
                                                break;
                                        }

                                        len = -1;
                                        mHandler.sendMessage(serverReceptor);
                                        Thread.sleep(100);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            default:
                break;
        }
    }

    /**
     * Method that matches the header byte array to the source linkedList src
     *
     * @param src
     * @param header
     * @return
     */
    private static byte[] matchHeader(LinkedList src, byte[] header) {
        byte[] res = null;
        int len = 0;
        for (int i = 0; i < src.size(); i++) {
            if ((byte) src.get(i) == header[0]) {
                if ((byte) src.get(i + 1) == header[1]) {
                    if ((byte) src.get(i + 2) == header[2]) {
                        if ((byte) src.get(i + 3) == header[3]) {
                            len = byteArrayToInt(getNextNByte(src, i + 4, 4));
                            res = new byte[len];
                            for (int j = 0; j < len; j++) {
                                res[j] = (byte) src.get(i + 8 + j);
                            }
                        } else {

                        }
                    } else {

                    }
                } else {

                }
            } else {

            }
        }
        deletPrev(src, len);
        return res;

    }

    private static LinkedList deletPrev(LinkedList src, int len) {
        for (int i = 0; i < len + 8; i++) {
            src.remove(0);
        }
        return src;
    }

    /**
     * Method that turns byte array to an integer.
     *
     * @param b
     * @return
     */
    private static int byteArrayToInt(byte[] b) {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;
            value += (b[i] & 0x000000FF) << shift;
        }
        return value;
    }

    /**
     * Method that gets you the next 4 bytes into a byte array following the
     * INDEX (INCLUSIVE).
     *
     * @param index, src
     * @return
     */
    private static byte[] getNextNByte(LinkedList src, int index, int n) {
        byte[] result = new byte[n];
        for (int i = 0; i < n; i++) {
            result[i] = (byte) src.get(index + i);
        }
        return result;
    }

    /**
     * Method that enables the aircraft to go up when the button is pressed down.
     */
    public void touchDown() {
        mOnlyForVerticalFlag = true;
        mFlyingUp = true;
        float yawJoyControlMaxSpeed = DJIFlightControllerDataType.DJIVirtualStickYawControlMaxAngularVelocity;

        mYaw = (float) (yawJoyControlMaxSpeed * 0);

        ultimateUpperHeight = seekBarUpperHeight;
        safetyUpperHeight = ultimateUpperHeight - 3;

        if (null == mSendVirtualStickDataTimer) {
            mSendVirtualStickDataTaskOnlyForVertical = new SendVirtualStickDataTaskOnlyForVertical();
            mSendVirtualStickDataTimer = new Timer();
            mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTaskOnlyForVertical, 0, 20);
        }
    }

    /**
     * Method that enables the aircraft to go down when the button press is released.
     */
    public void touchUp() {
        mOnlyForVerticalFlag = true;
        mFlyingUp = false;
        mThrottle *= -1f;
        ultimateLowerHeight = seekBarLowerHeight;
        safetyLowerHeight = ultimateLowerHeight - 3;
    }


    /**
     * Method for enabling for the virtual stick mode.
     */
    public void enableVirtualStick() {
        mOnlyForVerticalFlag = false;
        DJISampleApplication.getAircraftInstance().
                getFlightController().enableVirtualStickControlMode(
                new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Utils.showDialogBasedOnError(getContext(), djiError);
                    }
                }
        );
    }

    /**
     * Method for disabling the virtual stick mode.
     */
    public void disableVirtualStick() {
        DJISampleApplication.getAircraftInstance().
                getFlightController().disableVirtualStickControlMode(
                new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Utils.showDialogBasedOnError(getContext(), djiError);
                    }
                }
        );
    }

    /**
     * Method for taking off the aircraft.
     */
    public void takeoff() {
        mOnlyForVerticalFlag = false;
        DJISampleApplication.getAircraftInstance().getFlightController().takeOff(
                new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Utils.showDialogBasedOnError(getContext(), djiError);
                    }
                }
        );
    }

    /**
     * Method that returns the local IP Address as a String.
     * @return IP Address.
     */
    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                                + inetAddress.getHostAddress() + "\n";
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    private class SendVirtualStickDataTask extends TimerTask {
        @Override
        public void run() {
            if (DJIModuleVerificationUtil.isFlightControllerAvailable()) {
                DJISampleApplication.getAircraftInstance().
                        getFlightController().sendVirtualStickFlightControlData(
                        new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(
                                mPitch, mRoll, mYaw, mThrottle
                        ), new DJIBaseComponent.DJICompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {

                            }
                        }
                );
            }
        }
    }


    private class SendVirtualStickDataTaskOnlyForVertical extends TimerTask {
        @Override
        public void run() {

            mCurrentAltitude = DJISampleApplication.getAircraftInstance().getFlightController().getCurrentState().getAircraftLocation().getAltitude();
            mhandler.post(new Runnable() {
                @Override
                public void run() {
                    //mSmallText.setText("mCurrentAltitude: " + mCurrentAltitude + "\n mPositionAltitude: " + mPositionAltitude + "\n mTargetAltitude: " + mTargetAltitude + "\nzVelocity: " + mThrottle);
                    mSmallText.setText("mCurrentAltitude: " + mCurrentAltitude + "\n safetyLowerHeight: " + safetyLowerHeight
                            + "\n ultimateLowerHeight: " + ultimateLowerHeight + "\n safetyUpperHeight: " + safetyUpperHeight + "\n ultimateUpperHeight: " + ultimateUpperHeight
                            + "\n mThrottle: " + mThrottle);
                }
            });
            if (mOnlyForVerticalFlag) {
                if (mFlyingUp) {
                    if (mCurrentAltitude <= ultimateUpperHeight) {
                        Log.d("", "Whether comes into the Send mThrottle");
                        if (DJIModuleVerificationUtil.isFlightControllerAvailable()) {
                            float verticalJoyControlMaxSpeed = DJIFlightControllerDataType.DJIVirtualStickVerticalControlMaxVelocity;
                            mThrottle = (float) (verticalJoyControlMaxSpeed * velocityFactor);
                            setVelocity();
                            DJISampleApplication.getAircraftInstance().
                                    getFlightController().sendVirtualStickFlightControlData(
                                    new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(
                                            mPitch, mRoll, mYaw, mThrottle
                                    ), new DJIBaseComponent.DJICompletionCallback() {
                                        @Override
                                        public void onResult(DJIError djiError) {

                                        }
                                    }
                            );
                        }
                    }

                    if ((mCurrentAltitude > ultimateUpperHeight)) {
                        if (DJIModuleVerificationUtil.isFlightControllerAvailable()) {
                            setPosition();
                            DJISampleApplication.getAircraftInstance().
                                    getFlightController().sendVirtualStickFlightControlData(
                                    new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(
                                            mPitch, mRoll, mYaw, safetyUpperHeight
                                    ), new DJIBaseComponent.DJICompletionCallback() {
                                        @Override
                                        public void onResult(DJIError djiError) {

                                        }
                                    }
                            );
                        }
                    }
                }

                if (!mFlyingUp) {
                    if (mCurrentAltitude >= ultimateLowerHeight) {
                        if (DJIModuleVerificationUtil.isFlightControllerAvailable()) {
                            setVelocity();
                            DJISampleApplication.getAircraftInstance().
                                    getFlightController().sendVirtualStickFlightControlData(
                                    new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(
                                            mPitch, mRoll, mYaw, mThrottle
                                    ), new DJIBaseComponent.DJICompletionCallback() {
                                        @Override
                                        public void onResult(DJIError djiError) {

                                        }
                                    }
                            );
                        }
                    }

                    if (mCurrentAltitude < ultimateLowerHeight) {
                        if (DJIModuleVerificationUtil.isFlightControllerAvailable()) {
                            setPosition();
                            DJISampleApplication.getAircraftInstance().
                                    getFlightController().sendVirtualStickFlightControlData(
                                    new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(
                                            mPitch, mRoll, mYaw, safetyLowerHeight
                                    ), new DJIBaseComponent.DJICompletionCallback() {
                                        @Override
                                        public void onResult(DJIError djiError) {

                                        }
                                    }
                            );
                        }
                    }
                }

            }


        }

        /**
         *  Method for setting the velocity mode.
         */
        private void setVelocity() {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setVirtualStickAdvancedModeEnabled(true);
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setHorizontalCoordinateSystem(
                            DJIFlightControllerDataType.DJIVirtualStickFlightCoordinateSystem.Ground);
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setYawControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickYawControlMode.AngularVelocity);
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setVerticalControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode.Velocity);

        }

        /**
         * Method for setting the position mode.
         */
        private void setPosition() {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setVirtualStickAdvancedModeEnabled(true);
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setHorizontalCoordinateSystem(
                            DJIFlightControllerDataType.DJIVirtualStickFlightCoordinateSystem.Ground);
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setYawControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickYawControlMode.AngularVelocity);
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setVerticalControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode.Position);
        }
    }
}
