package com.dji.sdk.sample.flightcontroller;

import android.app.Service;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.dji.sdk.sample.R;
import com.dji.sdk.sample.common.MainContent;
import com.dji.sdk.sample.common.SetViewWrapper;
import com.dji.sdk.sample.common.Utils;

import java.lang.reflect.InvocationTargetException;

import dji.thirdparty.eventbus.EventBus;

/**
 * Created by wangxiangjiu on 6/23/16.
 */
public class ChoiceView extends RelativeLayout implements View.OnClickListener {

    private Button btnToChooseServer;
    private Button btnToChooseClient;

    public ChoiceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context, attrs);
    }

    private void initUI(Context context, AttributeSet attrs) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);

        View content = layoutInflater.inflate(R.layout.choice, null, false);
        addView(content, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        btnToChooseClient = (Button) findViewById(R.id.choice_client);
        btnToChooseServer = (Button) findViewById(R.id.choice_server);

        btnToChooseClient.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) return;
                try {
                    EventBus.getDefault().post(new SetViewWrapper(MainContent.getView("flightcontroller", "VirtualStickView", getContext()), R.string.view_virtual_stick));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
            }
        });
        btnToChooseServer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) return;
                try {
                    EventBus.getDefault().post(new SetViewWrapper(MainContent.getView("flightcontroller", "ServerView", getContext()), R.string.view_server));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    @Override
    public void onClick(View v) {

    }
}
