package com.dji.sdk.sample.flightcontroller;

/**
 * Created by wangxiangjiu on 6/28/16.
 */
public class Command {

    /**
     * 0 ACTION_DOWN
     * 1 ACTION_UP
     * 2 VELOCITY_FACTOR
     * 3 TAKE_OFF_ALL
     * 4 ENABLE_ALL
     * 5 DISABLE_ALL
     * 6 UPPER_HEIGHT
     * 7 LOWER_HEIGHT
     * */
    public int cmdId;
    public float value;
    public int height;

    @Override
    public String toString() {
        return  "cmdId: " + cmdId + "value: " + value + "height: " + height;
    }
}
